# Postgre prerequisite

| Postgre source version | AWS DMS version to use                | 
| :-------------         | :----------:                          |
|  9.x, 10.x, 11.x       | Use any available AWS DMS version.    |
|      12.x              | Use AWS DMS version 3.3.3 and above. |


---
# Working with self-managed PostgreSQL databases as a source in AWS DMS

* Make sure that you use a PostgreSQL database that is version 9.4.x or later.

* **For full-load plus CDC tasks or CDC-only tasks**, grant **superuser** permissions. **For full-load only tasks,** the user account needs **SELECT** permissions on tables to migrate them.

* ``idle_in_transaction_session_timeout in``, Don't end idle transactions when you use AWS DMS.

## For CDC

Set ``postgresql.conf``  configuration file:
* Set ``Set wal_level = logical.``
``
wal_level (enum)
wal_level determines how much information is written to the WAL. The default value is **minimal**, which writes only the information needed to recover from a crash or immediate shutdown. **replica** adds logging required for WAL archiving as well as information required to run read-only queries on a standby server. Finally, **logical** adds information necessary to support logical decoding.
``
* Set ``max_replication_slots`` to a value greater than ``1``. (Set value according to the number of tasks that you want to run)
* Set ``max_wal_senders`` to a value greater than ``1``. (Set the concurrent task that can run)
* Set ``wal_sender_timeout =0``. (recommended to set = 0 as to turn off timeout mechanism).
* Remember to **restart** ``(sudo) service postgresql restart`` postgre server.

---
# Working with AWS-managed PostgreSQL databases as DMS sources

## Prerequisites for using an AWS-managed PostgreSQL database as a DMS source

Do following steps:
* Use the AWS master user account for the PostgreSQL DB instance as the user account for the PostgreSQL source endpoint. If another account is used beside master, the account must have ``rds_superuser role`` and the ``rds_replication role``.
* If source database is in a virtual private cloud (VPC), choose the VPC security group that provides access to the DB instance where the database resides.

## Enabling CDC with AWS-managed PostgreSQL DB instances with AWS DMS


| Aurora Postgre source version | AWS DMS full load support      |  AWS CDC support 
| :-------------                | :----------:                   |:------:
| version 2.1 with postgreSQL 10.5 compability (or lower)        | yes    | no
| version 2.2 with PostgreSQL 10.6 compability (or higher)       | yes    | no|


**To enable logical replication for an RDS PostgreSQL DB instance**
1. use aws master account
2. set ``rds.logical_replication`` in DB group parameter to 1. ``wal_level``, ``max_wal_senders``, ``max_replication_slots``, and ``max_connections`` parameters. These parameter changes can increase write ahead log (WAL) generation, so only set ``rds.logical_replication`` when you use logical replication slots.
3. Set the ``wal_sender_timeout`` parameter to 0.

---
# Things to be noted during migration

## Using DMS

When performing data type migration, be aware of the following:

* In some cases, the **PostgreSQL NUMERIC(p,s)** data type doesn't specify any precision and scale. 

* A table with an **ARRAY data type** must have a **primary key**. A table with an ARRAY data type missing a primary key gets suspended during full load.

