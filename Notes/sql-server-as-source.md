# SQL Server in Linux

## Check sql server status
systemctl status mssql-server

## Restart sql server 
sudo systemctl restart mssql-server

# SQLCMD command

## Check Version complete
>SELECT @@VERSION

## Show server name
>SELECT @@SERVERNAME

## Show db name
>Select db_Name()