referal link https://dailyscrawl.com/how-to-install-postgresql-on-amazon-linux-2/

# Installation guide

## Add the pgdg repository in Amazon Linux
sudo amazon-linux-extras install postgresql10 vim epel

## Install postgresql-10
sudo yum install -y postgresql-server postgresql-devel
sudo /usr/bin/postgresql-setup --initdb

## Start the postgres service
sudo systemctl enable postgresql
sudo systemctl start postgresql

## Check status
sudo systemctl status postgresql

## restart postgre
sudo service postgresql restart

## Set PostgreSQL admin usernames password
sudo su - postgres 

## Connect remotely
psql --host=felix-postgre.c3ooadacqesg.us-east-2.rds.amazonaws.com
     --port=5432 -username=postgresFelix --password 

psql --host=felix-postgre.c3ooadacqesg.us-east-2.rds.amazonaws.com --port=5432 --username=postgresFelix --password --dbname=dvdrental

## Connect locally with username and database name
psql -U ``username`` -d ``db-name``

``psql -c "alter user postgres with password 'password'" ``

## connect using username and password
psql -d mydb -U myuser

# Postgre command inside 

## check connection info
postgre-# \conninfo

## show username list
=# SELECT usename FROM pg_user;


## show privilege on a user;
SELECT table_catalog, table_schema, table_name, privilege_type FROM information_schema.table_privileges WHERE grantee = 'example_user';

## viewing existing user permissions
=# \du


## check whether "tester" has a privilege

``
SELECT usename AS role_name,
  CASE 
     WHEN usesuper AND usecreatedb THEN 
	   CAST('superuser, create database' AS pg_catalog.text)
     WHEN usesuper THEN 
	    CAST('superuser' AS pg_catalog.text)
     WHEN usecreatedb THEN 
	    CAST('create database' AS pg_catalog.text)
     ELSE 
	    CAST('' AS pg_catalog.text)
  END role_attributes
FROM pg_catalog.pg_user
WHERE usename = 'tester';
ORDER BY role_name desc;
``

## check whether the user has what role attached to it
``
 SELECT 
      r.rolname, 
      r.rolsuper, 
      r.rolinherit,
      r.rolcreaterole,
      r.rolcreatedb,
      r.rolcanlogin,
      r.rolconnlimit, r.rolvaliduntil,
  ARRAY(SELECT b.rolname
        FROM pg_catalog.pg_auth_members m
        JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid)
        WHERE m.member = r.oid) as memberof
, r.rolreplication
, r.rolbypassrls
FROM pg_catalog.pg_roles r
WHERE r.rolname = 'user_that_want_to_be_checked'
ORDER BY 1;
``

*hbs possible location*
``
 sudo vim /var/lib/pgsql/data/pg_hba.conf
``

*config possible location*
``
sudo vim /var/lib/pgsql/data/postgresql.conf
``

# from psql command

## show table in database
SELECT * FROM pg_catalog.pg_tables;

## connect to db
-# \c db-name
``for example: -# \c dvdrental``

## show table after connected to db
-# \dt

## show db in cluster
-# \l
## show schema in db
-# \dn

## show table inside schema
-# \dt schema-name.


## password for tester2 : password
## password for postgresFelix: Password1!
## password for john : Password1!

## create new username
CREATE USER youruser WITH ENCRYPTED PASSWORD 'yourpass';

## list table without foreign key
select tbl.table_schema, 
       tbl.table_name
from information_schema.tables tbl
where table_type = 'BASE TABLE'
  and table_schema not in ('pg_catalog', 'information_schema')
  and not exists (select 1 
                  from information_schema.key_column_usage kcu
                  where kcu.table_name = tbl.table_name 
                    and kcu.table_schema = tbl.table_schema)


## Alter setting
ALTER SYSTEM SET wal_level = replica;

## Check aurora setting
select AURORA_VERSION();
