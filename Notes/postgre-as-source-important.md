# Documentation of DMS Source pre-made-checker

### How to run the script and work-flow

1. The postgre.sh will be run by using the command ``./postgre.sh``
2. It will ask the user to enter their local postgre username and the database that want to be checked with their password
3. If it successfully connected to the database, the checker will then ask the user if they want to migrate their data using full-load migration or with CDC 
4. The result will be shown and green word will be outputted if the prerequisite data is suitable with the postgre as source 


### **Things to be noted**

* To use ``pglogical``, add ``shared_preload_libraries = 'pglogical'`` to the **postgresql.conf** file.
* The shell script only work with **locally** installed postgresql that want to be migrated to AWS Cloud 
* The shell script will only check the basic requirement needed **before** making **DMS instance** to spot error 
* The things that the shell script check :
  * For full-load
    1. Postgresql version
    2. Superuser attribute linked with user that want to perform replication
  * For CDC
    1. Postgresql version
    2. Superuser attribute linked with user that want to perform replication
    3. ``idle_in_transaction_session_timeout`` 
    4. ``Set wal_level = logical.``
    5. ``max_replication_slots``
    6. ``max_wal_senders``
    7. ``wal_sender_timeout``
    8. Check whether all table in the selected db have a **primary keys**

### **Warning**
* The script does not check the **ip of DMS instance** because it checks basic *prerequisite* before making DMS instance.
* The script has not been able to check if the Database in the **RDS**
* The script also does not differentiate the CDC if the user want to use the native CDC start point. The script only check **logical replication**
* Using native CDC start points to set up a CDC load of a PostgreSQL source can **only be checked when the endpoint have been created** and it will not be checked by the script as it is a **extra connection attribute** in DMS console.
---

# Further reading for source endpoint of PostgreSQL 

### DMS version that can be used

| Postgre source version |        AWS DMS version to use         | 
| :-------------         | :--------------:                      |
|  9.x, 10.x, 11.x       | Use any available AWS DMS version.    |
|      12.x              | Use AWS DMS version 3.3.3 and above.  |

### Self-managed PostgreSQL databases as a source in AWS DMS

* Make sure that PostgreSQL database version is **version 9.4.x or later**. (the script can check this for you).

* **For full-load plus CDC tasks or CDC-only tasks**, grant **superuser** permissions. **For full-load only tasks,** the user account needs **SELECT** permissions on tables to migrate them.

* ``idle_in_transaction_session_timeout in``, Don't end idle transactions when you use AWS DMS.

### Prerequisites for using an AWS-managed PostgreSQL database as a DMS source
* Need to use **master user** (must have **rds_superuser role** and **rds_replication role**)
 
* If source database is in a **virtual private cloud (VPC)**, choose the VPC security group that **provides access to the DB instance**.

* When not using master account, please refer to the instructions:
  1. Choose the schema where the objects are to be created and **accessible** by ``No Priv``.
  2. login with user account beside master user.
  3. Create the table ``awsdms_ddl_audit`` by running the following command
    ```
    create table objects_schema.awsdms_ddl_audit
    (  
    c_key    bigserial primary key,
    c_time   timestamp,    -- Informational
    c_user   varchar(64),  -- Informational: current_user
    c_txn    varchar(16),  -- Informational: current transaction
    c_tag    varchar(24),  -- Either 'CREATE TABLE' or 'ALTER TABLE' or 'DROP TABLE'
    c_oid    integer,      -- For future use - TG_OBJECTID
    c_name   varchar(64),  -- For future use - TG_OBJECTNAME
    c_schema varchar(64),  -- For future use - TG_SCHEMANAME. For now - holds current_schema
    c_ddlqry  text         -- The DDL query associated with the current DDL event
    )                
    ```
     4. Create the function ``awsdms_intercept_ddl`` by running the following command 
    ```
   CREATE OR REPLACE FUNCTION objects_schema.awsdms_intercept_ddl()
  RETURNS event_trigger
    LANGUAGE plpgsql
    SECURITY DEFINER
  AS $$
  declare _qry text;
    BEGIN
  if (tg_tag='CREATE TABLE' or tg_tag='ALTER TABLE' or tg_tag='DROP TABLE') then
         SELECT current_query() into _qry;
         insert into objects_schema.awsdms_ddl_audit
         values
         (
         default,current_timestamp,current_user,cast(TXID_CURRENT()as varchar(16)),tg_tag,0,'',current_schema,_qry
         );
         delete from objects_schema.awsdms_ddl_audit;
    end if;
    END;
    $$;
    ```

> *objects_schema* will be replaced with the following 2 codes from above.

  5. Log out from ``NoPriv`` account and use user that has ``rds_superuser`` role assigned to it.
  6. Create event trigger ``awsdms_intercept_ddl`` with the following command.
  ``
    CREATE EVENT TRIGGER awsdms_intercept_ddl ON ddl_command_end 
    EXECUTE PROCEDURE objects_schema.awsdms_intercept_ddl();
  ``
  
  7. When all the steps is done , AWS DMS source endpoint can be useed with ``NoPriv`` account.

### Enabling change data capture (CDC) using logical replication

* Primary keys must have in each table (The script will check it for you).
* Below are the steps to configure ``pglogical`` extension
  * Create a pglogical extension on source PostgreSQL database:
    1. set the correct paramater: 
       a. For self-managed PostgreSQL : ``shared_preload_libraries= 'pglogical'``.
       b. For Amazon RDS PostgreSQL and Amazon Aurora PostgreSQL set ``shared_preload_libraries`` to **pglogical** in the **same RDS parameter group**.
    2. Run ``select * FROM pg_catalog.pg_extension`` to check whether pglogical has been installed.
    3. Enable CDC task using **native start point**
       a. Create replication slot with ``SELECT * FROM pg_create_logical_replication_slot('replication_slot_name', 'pglogical');`` 
       b. Create two replication sets, as shown following
        ``select pglogical.create_replication_set('replication_slot_name', true, false, false, true);`` and ``select pglogical.create_replication_set('replication_slot_name', false, true, true, false); ``

### Using native CDC start points to set up a CDC load of a PostgreSQL source

* Use ``slotName`` extra connection attribute to **existing logical replication slot** when endpoint is created. 
* To use native start point, follow the steps bellow
  * Identify the logical replication slot used by an earlier replication task by using ``pg_replication_slots`` and make sure **it does not have an active connection** and if it does, resolve and close before proceed.
  * Create a new source endpoint that includes the following extra connection attribute setting.
  * Use AWS CLI or AWS DMS API to create CDC-only task by using the following command 
   ```
   aws dms create-replication-task --replication-task-identifier postgresql-slot-name-test 
    --source-endpoint-arn arn:aws:dms:us-west-2:012345678901:endpoint:ABCD1EFGHIJK2LMNOPQRST3UV4 
    --target-endpoint-arn arn:aws:dms:us-west-2:012345678901:endpoint:ZYX9WVUTSRQONM8LKJIHGF7ED6 
    --replication-instance-arn arn:aws:dms:us-west-2:012345678901:rep:AAAAAAAAAAA5BB4CCC3DDDD2EE 
    --migration-type cdc --table-mappings "file://mappings.json" --cdc-start-position "4AF/B00000D0" 
    --replication-task-settings "file://task-pg.json"
   ```

### Question regarding the Native CDC start points and logical replication.

Both of them are the CDC method in migrating the database and the difference between them is that logical replication use **pglogical** extension and the effect in using that extension is duces downtime and help ensure that the target database is in sync with the source. However, Native CDC start points is another method of CDC using **timestamp** which can be set at what time or date the data in database need to be sync. The difference between them is one is not using time at a start while the other one use time to mark. Further information can be read in the website for reference.
* [Determining a CDC native start point.][dmssp]
* [Extra connection attributes when using PostgreSQL as a DMS source.][eca]
 
### Migrating from PostgreSQL to PostgreSQL using AWS DMS
* Directly use ``pg_dump`` migration tool in postgreSQL when the source and target are **both PostgreSQL**. However, only use ``pg_dump`` with the conditions of migrating entire database.
* If a PostgreSQL source database running on EC2 wants to be migrated from an Amazon RDS for PostgreSQL target, you can use the ``pglogical`` plugin.
> Add ``shared_preload_libraries = 'pglogical'`` to the **postgresql.conf**
 
 
 ### Reference
 * [Using PostgreSQL as source][poso] 
 * [Extra connection attributes when using PostgreSQL as a DMS source.][eca]
 
 
 
 
 
 
 
 [poso]:https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.PostgreSQL.html
 [eca]:https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.PostgreSQL.html#CHAP_Source.PostgreSQL.ConnectionAttrib
 [dmssp]:https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Task.CDC.html#CHAP_Task.CDC.StartPoint.Native