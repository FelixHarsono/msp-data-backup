## Change command to root
sudo su

## Install mysql 
- sudo wget https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
- sudo yum localinstall -mysql57-community-release-el7-11.noarch.rpm 
- sudo yum install mysql-community-server
- sudo systemctl start mysqld.service

## Get temporary password
- sudo grep 'temporary password' /var/log/mysqld.log


## Change password
- mysql_secure_installation

```
Remove anonymous users? (Press y|Y for Yes, any other key for No) : y
Success.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : n

Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y

```

## Create user and grant remote access

- sudo vi /etc/my.cnf 

<!-- add new line to my.cnf --> 
bind-address= [PUBLICIP of EC2]

## Start mysql
sudo systemctl start mysqld.service

## Restart mysql
sudo systemctl restart mysqld.service

## Check mysql status
sudo systemctl status mysqld.service

## Check mysql log
sudo vim /var/log/mysqld.log

## Enter mysql
mysql -u root -p

## Enter mysql through remote connection
mysql -h endpoint/ip -P 3306 -u username -p 
``Example: mysql -h felix-db.c3ooadacqesg.us-east-2.rds.amazonaws.com -P 3306 -u admin -p``

## Start mysql with database file
mysql -u root -p < mysqlsampledatabase.sql

---
# MYSQL important command

## Grand all user privilage
mysql > ``FLUSH PRIVILEGES;``

## Select database
mysql > use DBNAME  

## Show columns from table
mysql > SHOW COLUMNS FROM *Table name*;

## Show table
mysql > SHOW tables;

## Create user
CREATE USER 'migration-user'@'10.0.5.82' IDENTIFIED BY 'Password1!';
CREATE USER 'postgresFelix'@'10.0.1.200' IDENTIFIED BY 'password';


<!-- Reference website><-->
* https://phoenixnap.com/kb/mysql-remote-connection **mysql-remote-connection**
* https://www.digitalocean.com/community/tutorials/how-to-create-a-new-user-and-grant-permissions-in-mysql **create and grant user**
* https://www.cyberciti.biz/faq/unix-linux-mysqld-server-bind-to-more-than-one-ip-address/#:~:text=Long%20answer,map%20onto%20multiple%20network%20interfaces. **mysql server bind  **
