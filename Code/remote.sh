#!/bin/sh
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

# function to check version
version_user_checker ()
{
    #variable declaration
    local majorvc=$1
    local Versionvc=$2
    local IP=$3
    local PASS=$4
    local USERNAME=$5 
    local DATABASE=$6 
    
    err=$false
    
    if [[ $majorvc -eq 10 ]]
    then
        #echo "masuk eq 10"
        minor=${Versionvc:3:1}
        minor=$(($minor + 0))
        if [[ $minor -eq 4 ]]
        then
        echo "your database is ${RED}not compatible ${NC} with DMS because of limitation error"
        err=$true
        fi
    fi
    
    if [ $err = $false]
    then
        if [[ $majorvc -le 9 ]]
        then
            #echo "masuk"
            if [ ${Versionvc:1:1} != "." ]
            then
                # echo ${Version:1:1}
                # echo "masuk tidak sama dengan titik"
                Vernum=${Versionvc:0:2}
                #echo $Vernum
                if [[ $Vernum -gt 9 ]]
                then
                # echo "masuk >9"
                printf "your postgresql is ${GREEN}compatible${NC} with DMS.\n"
                fi
            else
            if [[ $majorvc = 9 ]]
            then 
            # echo $majorvc
            # echo "masuk sama dengan 9"
            minor=${Versionvc:2:1}
            minor=$(($minor + 0))
                if [[ $minor < 5 ]]
                then
                printf "you need to ${RED}update${NC} your PostgreSQL version\n" 
                else
                printf "your postgresql is ${GREEN}compatible${NC} with DMS.\n"
                fi
            fi
            fi
        fi
    fi
    
     #check superuser status
    Access=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -tc " SELECT 
  ARRAY(SELECT b.rolname
        FROM pg_catalog.pg_auth_members m
        JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid)
        WHERE m.member = r.oid) as memberof
FROM pg_catalog.pg_roles r
WHERE r.rolname = '$5'
ORDER BY 1;")
    
    
    
    IFS=',' read -r -a array <<< $Access
    len=${#array[@]}
    #echo $len
    last=$((len - 1))
    #echo $last
    
    
    rdssu=$false;
    rdsre=$false;
    
    for(( i=0 ; i<$len ; i++ )); 
    do
        if [[ i -eq 0 ]]
        then
        array[i]=`echo ${array[i]} | sed "s/{//g"`
        # printf "${array[i]}\n"
        else if [[ i -eq $last ]]
        then
        array[i]=`echo ${array[i]} | sed "s/}//g"`
        # printf "${array[i]}\n"
        fi
        fi
        
        if [ ${array[i]} = "rds_superuser" ]
        then 
        rdssu=$true
        fi
        
        if [ ${array[i]} = "rds_replication" ]
        then
        rdsre=$true
        fi
        
    done
    
    
    if [ $rdssu ] -a [ $rdsre ]
    then
    printf "Your username has ${GREEN}correct${NC} authorization.\n"
    else
    printf "${RED}Please check your authorization again.${NC} You need ${RED}rds_superuser${NC} and ${RED}rds_replication${NC} roles to use DMS.\n"
    fi
    
    printf "\n"
    
}

version_cdc_checker ()
{
    local majorvc=$1
    local Versionvc=$2
    local IP=$3
    local PASS=$4
    local USERNAME=$5 
    local DATABASE=$6 
    local aurora=$7
    
    if [ $aurora = "yes" ]
    then
        #echo "benar"
        acek="true"
    else
        acek="false"
    fi
    
    #echo $acek
    
    if [[ $majorvc -eq 10 ]]
    then
        #echo "masuk eq 10"
        minor=${Versionvc:3:1}
        minor=$(($minor + 0))
        if [[ $minor -eq 4 ]]
        then
        echo "your database is ${RED}not compatible ${NC} with DMS because of limitation error"
        printf " Your postgresql version = $Versionvc.\n"
        exit
        fi
    fi
    
    #for RDS
    #echo "${acek}"
    if [ $acek = "false" ] 
    then 
        #echo "acek false, bukan aurora"
        if [[ $majorvc -le 9 ]]
        then
            #echo "masuk"
            if [ ${Versionvc:1:1} != "." ]
            then
                Vernum=${Versionvc:0:2}
                if [[ $Vernum -gt 9 ]]
                then
                printf "Your postgresql is ${GREEN}compatible${NC} with DMS."
                printf " Your postgresql version = $Versionvc.\n"
                fi
            else
            if [[ $majorvc = 9 ]]
            then 
            minor=${Versionvc:2:1}
            minor=$(($minor + 0))
                if [[ $minor < 4 ]]
                then
                printf "you need to ${RED}update${NC} your PostgreSQL version."
                printf " Your postgresql version = $Versionvc.\n"
                else
                printf "your postgresql is ${GREEN}compatible${NC} for CDC."
                printf " Your postgresql version = $Versionvc.\n"
                fi
            fi
            fi
        fi
    #for aurora
    else
        if [[ $majorvc -le 10 ]]
        then
            #echo "masuk"
            if [ ${Versionvc:1:1} = "." ] || [  ${Versionvc:2:1} = "." ]
            then
                printf "Your postgresql is ${RED}not compatible${NC} for CDC."
                printf "Your postgresql version = $Versionvc.\n"
            else
            majorvc=${Versionvc:0:2}
            majorvc=$(($major + 0))
            if [[ $majorvc -eq 10 ]]
            then 
            minor=${Versionvc:3:1}
            minor=$(($minor + 0))
                if [[ $minor > 5 ]]
                then
                printf "Your postgresql is ${GREEN}compatible${NC} with CDC." 
                printf " Your postgresql version = $Versionvc.\n"
                else
                printf "Your postgresql is ${RED}not compatible${NC} with CDC."
                printf " Your postgresql version = $Versionvc.\n"
                fi
            fi
            fi
        fi
    
        AVC=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -tc "select AURORA_VERSION();" )
        amajor=${AVC:0:1}
        amajor=$(($amajor + 0))
        if [ $amajor -lt 2 ]
        then
        printf "Your Aurora version is ${RED}not compatible${NC} to perfom CDC."
        printf " Your Aurora version = $AVC.\n"
        else
        printf "Your Aurora version is ${GREEN}compatible${NC} to perfom CDC."
        printf " Your Aurora version = $AVC.\n"
        fi
    fi
    
    #check superuser status
    Access=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -tc " SELECT 
  ARRAY(SELECT b.rolname
        FROM pg_catalog.pg_auth_members m
        JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid)
        WHERE m.member = r.oid) as memberof
FROM pg_catalog.pg_roles r
WHERE r.rolname = '$5'
ORDER BY 1;")
    
    
    
    IFS=',' read -r -a array <<< $Access
    len=${#array[@]}
    #echo $len
    last=$((len - 1))
    #echo $last
    
    
    rdssu=$false;
    rdsre=$false;
    
    for(( i=0 ; i<$len ; i++ )); 
    do
        if [[ i -eq 0 ]]
        then
        array[i]=`echo ${array[i]} | sed "s/{//g"`
        # printf "${array[i]}\n"
        else if [[ i -eq $last ]]
        then
        array[i]=`echo ${array[i]} | sed "s/}//g"`
        # printf "${array[i]}\n"
        fi
        fi
        
        if [ ${array[i]} = "rds_superuser" ]
        then 
        rdssu=$true
        fi
        
        if [ ${array[i]} = "rds_replication" ]
        then
        rdsre=$true
        fi
        
    done
    
    
    if [ $rdssu ] -a [ $rdsre ]
    then
    printf "Your username has ${GREEN}correct${NC} authorization.\n"
    else
    printf "${RED}Please check your authorization again.${NC} You need ${RED}rds_superuser${NC} and ${RED}rds_replication${NC} roles to use DMS.\n"
    fi
    
    printf "\n"
}

# cdc prerequisite setting 
cdc_pre ()
{
    #variable declaration
    local IP=$1
    local PASS=$2
    local USERNAME=$3 
    local DATABASE=$4 
    
    # for max_replica_slot checker
    MRS=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -tc "show max_replication_slots;" )
    MRS=$(($MRS + 0))
    if [[ $MRS > 0 ]]
    then
        printf "max_replication_slots ${GREEN}OK${NC}.\n"
        
    else
        printf "max_replication_slots ${RED}need to be greater than 0${NC}."
        printf " Current setting max_replication_slot = ${YELLOW}$MRS${NC}\n"
    fi
    
    # for wal_value checker
    WAL=$(PGPASSWORD=$PASS psql --host=$IP --port=5432  -U $USERNAME -d $DATABASE -tc "show wal_level;")
    if [ $WAL != "logical" ]
    then
        printf "wal_level need to be ${RED}logical${NC}."
        printf " Current setting wal_level = ${YELLOW}$WAL${NC}\n"
    else
        printf "wal_level ${GREEN}OK${NC}\n"
    fi
    
    # for max_wal_senders value checker
    MWS=$(PGPASSWORD=$PASS psql --host=$IP --port=5432  -U $USERNAME -d $DATABASE -tc "show max_wal_senders;")
    MWS=$(($MWS + 0))
    if [[ $MWS -gt 0 ]]
    then
        printf "max_wal_senders ${GREEN}OK${NC}.\n"
    else
        printf "max_wal_senders ${RED}need to be greater than 0${NC}."
        printf " Current setting max_wal_senders = ${YELLOW}$MWS${NC}\n"
    fi
    
    # for wal_sender_timeout checker
    WST=$(PGPASSWORD=$PASS psql --host=$IP --port=5432  -U $USERNAME -d $DATABASE -tc "show wal_sender_timeout;")
    
    for (( i=0; i<${#WST}; i++ ))
    do
        x=${foo:$i:1}
        if [[ "$x" =~ [^a-zA-Z] ]]
        then
        isAlpha="true"
        fi
    done
    
    if [ $isAlpha="true" ] || [ $WST -gt 0 ] 
    then
        printf "wal_sender_timeout ${GREEN}OK${NC}.\n"
    else
        printf "wal_sender_timeout ${RED}need to be greater than 0${NC}."
        printf " Current setting wal_sender_timeout = ${YELLOW}$WST${NC}\n"
    fi
    
    isAlpha="false";
    
    # idle_in_transaction_session_timeout checker
    ITS=$(PGPASSWORD=$PASS psql --host=$IP --port=5432  -U $USERNAME -d $DATABASE -tc "show idle_in_transaction_session_timeout;")
    for (( i=0; i<${#ITS}; i++ ))
    do
        x=${foo:$i:1}
        if [[ "$x" =~ [^a-zA-Z] ]]
        then
        isAlpha="true"
        fi
    done
    
    if [ $isAlpha="true" ] || [ $ITS -eq 0 ]
    then
        printf "idle_in_transaction_session_timeout ${GREEN}OK${NC}\n"
    else
        printf "idle_in_transaction_session_timeout ${RED}need to 0${NC}."
        printf " Current setting idle_in_transaction_session_timeout = ${YELLOW}$ITS${NC}\n"
        
    fi
    
    # primary key checker
    NPK=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -tc "select tbl.table_schema, 
       tbl.table_name
from information_schema.tables tbl
where table_type = 'BASE TABLE'
  and table_schema not in ('pg_catalog', 'information_schema')
  and not exists (select 1 
                  from information_schema.key_column_usage kcu
                  where kcu.table_name = tbl.table_name 
                    and kcu.table_schema = tbl.table_schema);")
    #echo $NPK
    
    if [ -z "$NPK" ]
    then
        printf "All your tables have ${GREEN}primary key ${NC} \n"
    else
        printf "Make sure that all you tables have ${RED} primary key ${NC} \n"
    fi
    
    printf "\n"
}

#-----------------------------------------------------------------------------------------
# main program
printf "\n"

echo "what is remote ip?"
read IP
echo "What is your username for migration?"
read USERNAME
echo "Which database you want to check ?"
read DATABASE
echo "Please enter your password"
read -s PASS
#echo $PASS

PASS=$PASS | xargs
#echo $PASS

EXIST=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -c "\q")
#echo $EXIST

if [ $? -gt 0 ]
then
exit
fi

DBN=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -tc "SELECT table_name
FROM information_schema.tables
WHERE table_schema = 'public';" | cut -d \| -f 1)
#echo $DBN

shopt -s extglob  # turn on extended glob
DBN=("${DBS[@]/#+([[:blank:]])/}")
    
len=${#DBN[@]}
for(( i=0 ; i<$len ; i++ )); 
do
    # DBN[i]=${DBN[i]} | xargs
    printf "${DBN[i]}\n"
done

#ask whether the user want to full-load or CDC
echo "Which type of DMS will you use to migrate your database ? (full / cdc)"
read TYPE

if [ $TYPE = "full" ]
then
    #login to postgresql
    #check 1, postgres version
    Version=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -tc "SHOW server_version;" )
    Version="${Version#"${Version%%[![:space:]]*}"}"

    #echo "this is shell $Version"
    printf "\n"
    echo "========== Please report when there are red text ==========="
    printf "\n"
    
    major=${Version:0:1}
    major=$(($major + 0))

    version_user_checker "$major" "$Version" "$IP" "$PASS" "$USERNAME" "$DATABASE"
    exit
    
#--------------------------------- cdc check ----------------------------------------------

elif [ $TYPE = "cdc" ]
then
    #login to postgresql
    #check 1, postgres version
    Version=$(PGPASSWORD=$PASS psql --host=$IP --port=5432 -U $USERNAME -d $DATABASE -tc "SHOW server_version;" )
    Version="${Version#"${Version%%[![:space:]]*}"}"

    #echo "this is shell $Version"
    echo "Is your database type using AWS Aurora ? (yes/no)"
    read aurora;
    
    printf "\n"
    echo "========== Please report when there are red text ==========="
    printf "\n"
    
    major=${Version:0:1}
    major=$(($major + 0))

    version_cdc_checker "$major" "$Version" "$IP" "$PASS" "$USERNAME" "$DATABASE" "$aurora"
    cdc_pre "$IP" "$PASS" "$USERNAME" "$DATABASE"
    
    # TPK=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "SELECT constraint_name, table_name, column_name, ordinal_position
    # FROM information_schema.key_column_usage
    # WHERE table_name = 'actor' ;" )
    # echo $TPK
    
    exit
    
fi