#!/bin/sh
RED='\033[0;31m'
YELLOW='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'

# function to check version
version_user_checker ()
{
    #variable declaration
    local majorvc=$1
    local Versionvc=$2
    local PASS=$3
    local USERNAME=$4 
    local DATABASE=$5 
    
    # echo $majorvc
    # echo $Versionvc
    if [[ $majorvc -le 9 ]]
    then
        #echo "masuk"
        if [ ${Versionvc:1:1} != "." ]
        then
            #echo ${Version:1:1}
            #echo "masuk tidak sama dengan titik"
            # Vernum=${Versionvc:0:1}
            Vernum=${Versionvc:0:2}
            #echo $Vernum
            if [[ $Vernum -gt 9 ]]
            then
            # echo "masuk >9"
            printf "your postgresql is ${GREEN}compatible${NC} with DMS.\n"
            fi
        else
        if [[ $majorvc = 9 ]]
        then 
        minor=${Versionvc:2:1}
        minor=$(($minor + 0))
            if [[ $minor < 4 ]]
            then
            printf "you need to ${RED}update${NC} your PostgreSQL version\n" 
            else
            printf "your postgresql is ${GREEN}compatible${NC} with DMS.\n"
            fi
        fi
        fi
    fi
    
     #check superuser status
    Access=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "select usesuper from pg_user where usename = CURRENT_USER;")
    #echo $Access
    if [ $Access = "t" ]
    then
    printf "Your username has ${GREEN}correct${NC} authorization.\n"
    else
    printf "${RED}Please check your authorization again. ${NC}\n"
    fi
    
    printf "\n"
    
}

# cdc prerequisite setting 
cdc_pre ()
{
    #variable declaration
    local PASS=$1
    local USERNAME=$2 
    local DATABASE=$3 
    
    # for max_replica_slot checker
    MRS=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "show max_replication_slots;" )
    MRS=$(($MRS + 0))
    if [[ $MRS > 0 ]]
    then
        printf "max_replication_slots ${GREEN}OK${NC}.\n"
        
    else
        printf "max_replication_slots ${RED}need to be greater than 0${NC}."
        printf " Current setting max_replication_slot = ${YELLOW}$MRS${NC}\n"
    fi
    
    # for wal_value checker
    WAL=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "show wal_level;")
    if [ $WAL != "logical" ]
    then
        printf "wal_level need to be ${RED}logical${NC}."
        printf " Current setting wal_level = ${YELLOW}$WAL${NC}\n"
    else
        printf "wal_level ${GREEN}OK${NC}\n"
    fi
    
    # for max_wal_senders value checker
    MWS=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "show max_wal_senders;")
    MWS=$(($MWS + 0))
    if [[ $MWS -gt 0 ]]
    then
        printf "max_wal_senders ${GREEN}OK${NC}.\n"
    else
        printf "max_wal_senders ${RED}need to be greater than 0${NC}."
        printf " Current setting max_wal_senders = ${YELLOW}$MWS${NC}\n"
    fi
    
    # for wal_sender_timeout checker
    WST=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "show wal_sender_timeout;")
    if [ $WST = "1min" ] || [[ $WST -gt 0 ]]
    then
        printf "wal_sender_timeout ${GREEN}OK${NC}.\n"
    else
        printf "wal_sender_timeout ${RED}need to be greater than 0${NC}."
        printf " Current setting wal_sender_timeout = ${YELLOW}$WST${NC}\n"
    fi
    
    # idle_in_transaction_session_timeout checker
    ITS=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "show idle_in_transaction_session_timeout;")
    if [[ $ITS -gt 0 ]]
    then
        printf "idle_in_transaction_session_timeout ${RED}need to 0${NC}."
        printf " Current setting idle_in_transaction_session_timeout = ${YELLOW}$ITS${NC}\n"
    else
        printf "idle_in_transaction_session_timeout ${GREEN}OK${NC}\n"
    fi
    
    # primary key checker
    NPK=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "select tbl.table_schema, 
       tbl.table_name
from information_schema.tables tbl
where table_type = 'BASE TABLE'
  and table_schema not in ('pg_catalog', 'information_schema')
  and not exists (select 1 
                  from information_schema.key_column_usage kcu
                  where kcu.table_name = tbl.table_name 
                    and kcu.table_schema = tbl.table_schema);")
    # echo $NPK
    
    if [ -z "$NPK" ]
    then
        printf "All your tables have ${GREEN}primary key ${NC} \n"
    else
        printf "Make sure that all you tables have ${RED} primary key ${NC} \n"
    fi
    
    printf "\n"
}

target_check()
{
    local PASS=$1
    local USERNAME=$2 
    local DATABASE=$3
    
    #session replication role
    SRR=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "show session_replication_role;")
    if [ $SRR != "replica" ]
    then
    printf "session_replication_role needs to be ${RED}replica${NC}."
    printf " Current setting session_replication_role =${YELLOW}$SRR${NC}\n"
    else
    printf "session_replication_role ${GREEN}OK${NC}\n"
    fi
    
}

#-----------------------------------------------------------------------------------------
# main program
printf "\n"

echo "What is your username?"
read USERNAME
echo "Which database you want to check ?"
read DATABASE
echo "Please enter your password"
read -s PASS
#echo $PASS

PASS=$PASS | xargs

EXIST=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -c "\q")
#echo $EXIST

if [ $? -gt 0 ]
then
exit
fi

DBN=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "SELECT table_name
FROM information_schema.tables
WHERE table_schema = 'public';" )
#echo "this is 1st DBN"
#echo $DBN

# IFS=' ' read -r -a array <<< $DBN
# len=${#array[@]}
# #echo $len

# for(( i=0 ; i<$len ; i++ )); 
# do
#     array[i]=${array[i]} | xargs
#     printf "${array[i]}\n"
# done


#===================================================================

# use to divide list of strings to array

# for element in "${array[@]}"
# do
#     echo "$element"
# done

# shopt -s extglob  # turn on extended glob
# DBN=("${DBS[@]/#+([[:blank:]])/}")
    
# len=${#DBN[@]}
# echo $len
# for(( i=0 ; i<$len ; i++ )); 
# do
#     DBN[i]=${DBN[i]} | xargs
#     printf "${DBN[i]}\n"
# done

#====================================================================


#ask whether the user want to full-load or CDC
echo "What is the role type of the database that you want to check ? (source / target)"
read ROLE

if [ $ROLE = "source" ]
then
    echo "Which type of DMS will you use to migrate your database ? (full / cdc)"
    read TYPE
    
    if [ $TYPE = "full" ]
    then
        #login to postgresql
        #check 1, postgres version
        Version=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "SHOW server_version;" )
        Version="${Version#"${Version%%[![:space:]]*}"}"
    
        #echo "this is shell $Version"
        printf "\n"
        echo "========== Please report when there are red text ==========="
        printf "\n"
        
        major=${Version:0:1}
        major=$(($major + 0))
    
        version_user_checker "$major" "$Version" "$PASS" "$USERNAME" "$DATABASE"
        exit
        
    #--------------------------------- cdc check ----------------------------------------------
    
    elif [ $TYPE = "cdc" ]
    then
        #login to postgresql
        #check 1, postgres version
        Version=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "SHOW server_version;" )
        Version="${Version#"${Version%%[![:space:]]*}"}"
    
        #echo "this is shell $Version"
        
        printf "\n"
        echo "========== Please report when there are red text ==========="
        printf "\n"
        
        major=${Version:0:1}
        major=$(($major + 0))
    
        version_user_checker "$major" "$Version" "$PASS" "$USERNAME" "$DATABASE"
        cdc_pre "$PASS" "$USERNAME" "$DATABASE"
        
        # TPK=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "SELECT constraint_name, table_name, column_name, ordinal_position
        # FROM information_schema.key_column_usage
        # WHERE table_name = 'actor' ;" )
        # echo $TPK
      
        
        exit
        
    fi
else
    Version=$(PGPASSWORD=$PASS psql -U $USERNAME -d $DATABASE -tc "SHOW server_version;" )
    Version="${Version#"${Version%%[![:space:]]*}"}"

    #echo "this is shell $Version"
    
    printf "\n"
    echo "========== Please report when there are red text ==========="
    printf "\n"
    
    major=${Version:0:1}
    major=$(($major + 0))

    version_user_checker "$major" "$Version" "$PASS" "$USERNAME" "$DATABASE"
    target_check "$PASS" "$USERNAME" "$DATABASE"
    
    printf "\n"
fi